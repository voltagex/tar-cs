﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace tar_cs.Tests
{
    [TestClass]
    public class SimpleTests
    {

        private Stream gnuStream;
        private Stream bsdStream;

        [TestInitialize]
        public void SetUp()
        {
            var resourceNames = Assembly.GetExecutingAssembly().GetManifestResourceNames();
            string gnuTar = resourceNames.First(m => m.EndsWith("gnu-hello.tar"));
            string bsdTar = resourceNames.First(m => m.EndsWith("bsd-hello.tar"));

            gnuStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(gnuTar);
            bsdStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(bsdTar);
        }

        [TestMethod]
        public void GNU()
        {
            List<ITarHeader> list = new List<ITarHeader>();
            TarReader reader = new TarReader(gnuStream);

            while (reader.MoveNext(true))
            {
                list.Add(reader.FileInfo);
                Trace.WriteLine($"{reader.FileInfo.EntryType}: {reader.FileInfo.FileName}");
            }

            Assert.IsNotNull(list.FirstOrDefault(x => x.FileName == "hello/"));
            Assert.IsNotNull(list.FirstOrDefault(x => x.FileName == "hello/world/"));
            Assert.IsNotNull(list.FirstOrDefault(x => x.FileName == "hello/world/hello"));
        }

        [TestMethod]
        public void GNUAsFile()
        {
            List<ITarHeader> list = new List<ITarHeader>();
            var gnuFile = WriteToTempFile(gnuStream);
            TarReader reader = new TarReader(gnuFile);
            while (reader.MoveNext(true))
            {
                list.Add(reader.FileInfo);
                Trace.WriteLine($"{reader.FileInfo.EntryType}: {reader.FileInfo.FileName}");
            }

            Assert.IsNotNull(list.FirstOrDefault(x => x.FileName == "hello/"));
            Assert.IsNotNull(list.FirstOrDefault(x => x.FileName == "hello/world/"));
            Assert.IsNotNull(list.FirstOrDefault(x => x.FileName == "hello/world/hello"));
        }

        [TestMethod]
        public void BSD()
        {
            List<ITarHeader> list = new List<ITarHeader>();
            TarReader reader = new TarReader(bsdStream);

            while (reader.MoveNext(true))
            {
                list.Add(reader.FileInfo);
                Trace.WriteLine($"{reader.FileInfo.EntryType}: {reader.FileInfo.FileName}");
            }


            Assert.IsNotNull(list.FirstOrDefault(x => x.FileName == "hello/"));
            Assert.IsNotNull(list.FirstOrDefault(x => x.FileName == "hello/world/"));
            Assert.IsNotNull(list.FirstOrDefault(x => x.FileName == "hello/world/hello"));
        }

        [TestMethod]
        public void BSDAsFile()
        {
            List<ITarHeader> list = new List<ITarHeader>();
            var bsdFile = WriteToTempFile(bsdStream);
            TarReader reader = new TarReader(bsdFile);

            while (reader.MoveNext(true))
            {
                list.Add(reader.FileInfo);
                Trace.WriteLine($"{reader.FileInfo.EntryType}: {reader.FileInfo.FileName}");
            }

            Assert.IsNotNull(list.FirstOrDefault(x => x.FileName == "hello/"));
            Assert.IsNotNull(list.FirstOrDefault(x => x.FileName == "hello/world"));
            Assert.IsNotNull(list.FirstOrDefault(x => x.FileName == "hello/world/hello"));
        }


        [TestMethod]
        public void TestEOF()
        {
            List<ITarHeader> list = new List<ITarHeader>();
            var reader = new TarReader(gnuStream);
            while (!reader.EOF)
            {
                reader.MoveNext(true);
                list.Add(reader.FileInfo);
            }

            Assert.IsNotNull(list.FirstOrDefault(x => x.FileName == "hello/"));
            Assert.IsNotNull(list.FirstOrDefault(x => x.FileName == "hello/world/"));
            Assert.IsNotNull(list.FirstOrDefault(x => x.FileName == "hello/world/hello"));

        }

        internal string WriteToTempFile(Stream stream)
        {
            var path = Path.GetTempFileName();
            FileStream outputStream = new FileStream(path, FileMode.Open);
            stream.CopyTo(outputStream);
            stream.Close();
            outputStream.Close();

            return path;
        }



    }
}
